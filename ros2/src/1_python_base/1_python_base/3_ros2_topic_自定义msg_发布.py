#!/usr/bin/env python3
# coding:utf-8

import rclpy
from rclpy.node import Node
# import common # 使用common.msg.Num会运行失败, 提示找不到common.msg
from common.msg import Num

class ros2_publisher(Node):
    def __init__(self):
        super().__init__("ros2_publisher")
        # self.publisher = self.create_publisher(common.msg.Num,"/topic_num",10)
        self.publisher = self.create_publisher(Num,"/topic_num",10)
        timer_period = 0.5
        self.timer = self.create_timer(timer_period,self.time_callback)
        self.i = 0
    
    def time_callback(self):
        # msg = common.msg.Num()
        msg = Num()
        msg.num = self.i
        self.publisher.publish(msg)
        self.get_logger().info(f"send msg num {msg.num}")
        self.i += 1

def main(args=None):
    rclpy.init(args=args)
    ros2_pub = ros2_publisher()
    rclpy.spin(ros2_pub)
    ros2_pub.destory_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()