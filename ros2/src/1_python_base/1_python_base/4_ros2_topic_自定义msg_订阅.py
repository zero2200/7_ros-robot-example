#!/usr/bin/env python3
# coding:utf-8

import rclpy
from rclpy.node import Node
from common.msg import Num
import debugpy

class ros2_subscriber(Node):
    def __init__(self):
        super().__init__("ros2_subscriber")
        self.sub = self.create_subscription(Num,"/topic_num",self.listener_callback,10)
        
    def listener_callback(self,msg):
        self.get_logger().info(f"receive msg num {msg.num}")

def main(args=None):
    debugpy.listen(6688)
    rclpy.init(args=args)
    ros2_sub = ros2_subscriber()
    rclpy.spin(ros2_sub)
    ros2_sub.destory_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
