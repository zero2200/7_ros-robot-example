import rclpy
from rclpy.node import Node
from std_msgs.msg import String

class QoSListener(Node):
    def __init__(self):
        super().__init__('qos_listener')
        self.subscription = self.create_subscription(
            String,
            'qos_chatter',
            self.listener_callback,
            self.get_qos_profile()
        )

    def get_qos_profile(self):
        # 设置与发布者相匹配的QoS配置，这里也使用Best Effort
        qos_profile = rclpy.qos.QoSProfile(depth=10)
        qos_profile.reliability = rclpy.qos.ReliabilityPolicy.BEST_EFFORT
        qos_profile.history = rclpy.qos.HistoryPolicy.KEEP_LAST
        return qos_profile

    def listener_callback(self, msg):
        self.get_logger().info('Received: "%s"' % msg.data)

def main(args=None):
    rclpy.init(args=args)
    qos_listener = QoSListener()
    rclpy.spin(qos_listener)
    qos_listener.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()