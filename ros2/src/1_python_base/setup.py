import os
from glob import glob
from setuptools import find_packages, setup

package_name = '1_python_base'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*_launch.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='liuj',
    maintainer_email='liuj@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            # 'talker = py_pubsub.publisher_member_function:main',
            '1_topic_public = 1_python_base.1_topic_public:main',
            '2_topic_subscriber = 1_python_base.2_topic_subscriber:main',
            '3_ros2_topic_自定义msg_发布 = 1_python_base.3_ros2_topic_自定义msg_发布:main',
            '4_ros2_topic_自定义msg_订阅 = 1_python_base.4_ros2_topic_自定义msg_订阅:main',
            '9_ros2_param_set_get = 1_python_base.9_ros2_param_set_get:main',
            '11_Qos_topic_pub = 1_python_base.11_QoS_topic_发布:main',
            '12_QoS_topic_sub = 1_python_base.12_QoS_topic_订阅:main',
            '13_not_ros2_rclpy_init = 13_not_ros2_rclpy_init:main',
        ],
    },
)
