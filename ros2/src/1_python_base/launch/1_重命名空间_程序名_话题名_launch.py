import os
from glob import glob
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription(
        [
            Node(
                package="1_python_base",
                executable="1_topic_public",
                # namespace="release",
            ),
            # Node(
            #     package="1_python_base",
            #     executable="1_topic_public",
            #     namespace="debug",
            # ),
            Node(
                package="1_python_base",
                executable="1_topic_public",
                name="liuj_test_topic_public",
                namespace="debug",
                # remappings=[
                #     ("/topic", "/new_topic"),  # 设置话题的重映射
                # ],
            ),
        ]
    )
