import sys
import rclpy
from rclpy.node import Node


class Ros_Node(Node):
    def __init__(self):
        super().__init__("get_program_args")
        self.get_logger().info("get_program_args")
        if len(sys.argv) > 1:
            for arg in sys.argv:
                self.get_logger().info(f"arg: {arg}")


def main(args=None):
    rclpy.init(args=args)
    ros = Ros_Node()
    rclpy.spin(ros)
    ros.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
