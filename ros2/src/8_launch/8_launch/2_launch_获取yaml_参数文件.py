import rclpy
from rclpy.node import Node


class Ros_Node(Node):
    def __init__(self):
        super().__init__("get_param_file_yaml")
        # self.publisher_ = self.create_publisher(String, 'topic', 10)
        self.get_param()

    def get_param(self):
        # 必要: 声明参数  --> 不声明参数,参数文件中参数值不会生效
        self.declare_parameter("name", "default_name")
        self.declare_parameter("age", 0)
        self.declare_parameter("source", 0.0)

        # 获取参数值
        name = self.get_parameter('name').value
        age = self.get_parameter('age').value
        source = self.get_parameter('source').value
        print(f"name: {name}, age: {age}, source: {source}")
        self.get_logger().info(f'Name: {name} Age: {age} Source: {source}')


def main(args=None):
    rclpy.init(args=args)
    ros = Ros_Node()
    rclpy.spin(ros)
    ros.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
