import rclpy
from rclpy.node import Node


class Ros_Node(Node):
    def __init__(self):
        super().__init__("test_log_level")
        self.get_logger().debug("this debug")
        self.get_logger().info("this info")
        self.get_logger().warn("this warn")
        self.get_logger().error("this error")
        self.get_logger().fatal("this fatal")


def main(args=None):
    rclpy.init(args=args)
    ros = Ros_Node()
    rclpy.spin(ros)
    ros.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    main()
