#!/usr/bin/env python3
# coding:utf-8
import rclpy
from rclpy.node import Node

class MinimalNode(Node):
    def __init__(self):
        super().__init__('minimal_node')
        self.get_logger().info('Hello, world!')
        self.get_logger().info(f'Namespace is: {self.get_namespace()}')
        self.get_logger().info(f'Node name is: {self.get_name()}')
        self.get_logger().info(f'Fully qualified node name is: {self.get_fully_qualified_name()}')

def main(args=None):
    rclpy.init(args=args)
    minimal_node = MinimalNode()
    rclpy.spin_once(minimal_node)  # Spin once to allow the logger to output.
    minimal_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
