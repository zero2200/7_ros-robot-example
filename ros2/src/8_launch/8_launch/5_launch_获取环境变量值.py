#!/usr/bin/env python3
# coding:utf-8
import os

def main(args=None):
    user = os.getenv("USER")
    test_value = os.getenv("TEST_VALUE")
    test2_value = os.getenv("TEST2_VALUE")
    print(f"USER: {user} , TEST_VALUE: {test_value}, TEST2_VALUE: {test2_value}")

if __name__ == '__main__':
    main()
