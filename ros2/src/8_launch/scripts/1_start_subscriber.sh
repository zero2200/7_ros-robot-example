#!/bin/bash

# 加载ros2 工程环境
scriptDir=$(dirname $0)
workDir=$(cd $scriptDir/../../..; pwd)
source $workDir/setup.bash

mkdir -p /tmp/roslog
# stdbuf -oL -eL ros2 run 1_python_base 2_topic_subscriber > /tmp/roslog/2_sub.log 2>&1 # --OK
stdbuf -oL -eL ros2 run 1_python_base 2_topic_subscriber &> /tmp/roslog/2_sub.log   # --OK
