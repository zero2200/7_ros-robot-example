import os
from glob import glob
from setuptools import find_packages, setup

package_name = "8_launch"

setup(
    name=package_name,
    version="0.0.0",
    packages=find_packages(exclude=["test"]),
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
        (os.path.join('share', package_name), glob('launch/*_launch.py')),
        (os.path.join('share', package_name), glob('config/*.yaml')),
        (os.path.join('share', package_name), glob('config/config.json')),
        (os.path.join('share', package_name), glob('scripts/*.sh')),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="liuj",
    maintainer_email="lj1637664504@outlook.com",
    description="TODO: Package description",
    license="TODO: License declaration",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "1_launch_获取命令行参数 = 8_launch.1_launch_获取命令行参数:main",
            "2_launch_获取yaml_参数文件 = 8_launch.2_launch_获取yaml_参数文件:main",
            "3_launch_获取yaml_参数文件_不声明 = 8_launch.3_launch_获取yaml_参数文件_不声明:main",
            '4_get_process_namespace_nodeName = 8_launch.4_获取_命名空间namespace_节点名nodeName:main',
            '5_launch_获取环境变量值 = 8_launch.5_launch_获取环境变量值:main',
            '6_launch_loglevel = 8_launch.6_launch_loglevel_日志等级输出:main',
            '12_不同型号_版本参数_launch = 8_launch.12_不同型号_版本参数_launch:main',
        ],
    },
)
