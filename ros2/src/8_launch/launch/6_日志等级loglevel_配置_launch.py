from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    # 设置日志级别
    log_level = "warn"  # 可以设置为 'debug', 'info', 'warn', 'error', 'fatal'
    node = Node(
        package="8_launch",
        executable="6_launch_loglevel",
        output="both",
        # arguments=["--ros-args", "--log-level", log_level], # --OK
        ros_arguments=["--log-level", log_level],  # --OK
        # arguments=[('--ros-args --log-level debug')], # --NG 不生效
        # arguments=["--ros-args", "--log-level", f"6_launch_loglevel:={log_level}"], # --NG 不生效
    )
    return LaunchDescription([node])


# log_level = "warn" 时:
# this warn
# this error
# this fatal
