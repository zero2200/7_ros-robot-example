from launch import LaunchDescription
from launch.actions import TimerAction
from launch_ros.actions import Node


def generate_launch_description():
    node1 = Node(
        package="8_launch",
        executable="4_get_process_namespace_nodeName",
        output="screen",
    )
    node2 = Node(
        package="8_launch",
        executable="4_get_process_namespace_nodeName",
        output="screen",
        name="test_get_namespace_nodeName",
        namespace="test",
    )
    timer_node2 = TimerAction(period=5.0, actions=[node2])  # 5秒后启动 node2

    return LaunchDescription([node1, timer_node2])
