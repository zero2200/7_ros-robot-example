from launch import LaunchDescription
from launch.actions import ExecuteProcess
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    pkg_dir = get_package_share_directory("8_launch")
    print("pkg_dir", pkg_dir)
    return LaunchDescription(
        [
            Node(
                package="1_python_base",
                executable="1_topic_public",
                output="log",
                prefix=['stdbuf -oL -eL'],
                arguments=['&>', '/tmp/1.log'],
            ),
            # ExecuteProcess(
            #     # cmd=["/home/liuj/3_work/7_ros-robot-example/ros2/install/8_launch/share/8_launch/1_start_subscriber.sh"], # --OK
            #     cmd=[f"{pkg_dir}/1_start_subscriber.sh"],
            #     output="log",
            # ),
        ]
    )
