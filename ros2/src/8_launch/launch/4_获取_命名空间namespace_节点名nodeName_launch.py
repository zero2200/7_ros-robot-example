from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='8_launch',
            executable='4_get_process_namespace_nodeName',
            output='screen',
        ),
        Node(
            package='8_launch',
            executable='4_get_process_namespace_nodeName',
            output='screen',
            name='test_get_namespace_nodeName',
            namespace='test',
        )
    ])