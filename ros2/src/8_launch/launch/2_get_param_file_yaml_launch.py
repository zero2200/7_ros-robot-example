import os
from glob import glob
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():
    param_config = os.path.join(get_package_share_directory('8_launch'), '2_param.yaml')
    print(f"param_config: {param_config}")
    return LaunchDescription([
        Node(
            package='8_launch',
            executable='2_launch_获取yaml_参数文件',
            output='screen',
            parameters=[param_config]
        )
    ])