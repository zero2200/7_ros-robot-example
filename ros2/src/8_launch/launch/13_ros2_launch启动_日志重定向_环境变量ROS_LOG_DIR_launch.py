from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription(
        [
            Node(
                package="1_python_base",
                executable="1_topic_public",
                output = "log",
                additional_env={'ROS_LOG_DIR': '/tmp/roslog/1_pub',"RCUTILS_LOGGING_BUFFERED_STREAM": "0"}
            ),
            Node(
                package="1_python_base",
                executable="2_topic_subscriber",
                output = "log",
                additional_env={'ROS_LOG_DIR': '/tmp/roslog/2_sub',"RCUTILS_LOGGING_BUFFERED_STREAM": "0"}
            ),
        ]
    )
