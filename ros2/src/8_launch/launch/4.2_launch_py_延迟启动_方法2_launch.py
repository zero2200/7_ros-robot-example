import time
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import TimerAction, ExecuteProcess


def generate_launch_description():
    ld = LaunchDescription()

    node1 = Node(
        package="8_launch",
        executable="4_get_process_namespace_nodeName",
        output="screen",
    )
    node2 = Node(
        package="8_launch",
        executable="4_get_process_namespace_nodeName",
        output="screen",
        name="test_get_namespace_nodeName",
        namespace="test",
    )

    # 添加节点到启动描述
    ld.add_action(node1)

    # 设置延迟启动
    delay_seconds = 5.0  # 延迟5秒
    ld.add_action(TimerAction(period=delay_seconds, actions=[node2]))

    return ld
