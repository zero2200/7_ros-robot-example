from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import SetEnvironmentVariable


def generate_launch_description():
    ENV1 = SetEnvironmentVariable(name="TEST_VALUE", value="abc123")
    node1 = Node(
        package="8_launch",
        executable="5_launch_获取环境变量值",
        output="screen",
    )

    env2 = SetEnvironmentVariable(name="TEST_VALUE", value="def456")
    node2 = Node(
        package="8_launch",
        namespace="test",
        executable="5_launch_获取环境变量值",
        output="screen",
    )

    return LaunchDescription(
        [
            ENV1,
            node1,

            env2,
            node2,
        ]
    )

#  运行结果:
# [5_launch_获取环境变量值-1] USER: liuj , TEST_VALUE: abc123
# [5_launch_获取环境变量值-2] USER: liuj , TEST_VALUE: def456
# 