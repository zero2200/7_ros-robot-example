import os
from glob import glob
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    return LaunchDescription(
        [
            Node(
                package="8_launch",
                executable="1_launch_获取命令行参数",
                output="screen",
                arguments=["-o", "name", "-n", "--age"],
            )
        ]
    )
