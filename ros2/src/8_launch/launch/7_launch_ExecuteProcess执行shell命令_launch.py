from launch import LaunchDescription
from launch.actions import ExecuteProcess


def generate_launch_description():
    cmd_Node = ExecuteProcess(
        cmd=["ls", "/"],
        output="screen",
        shell=True,
    )

    # 启动节点
    launch_description = LaunchDescription(
        [
            cmd_Node,
        ]
    )
    return launch_description

# 执行结果:
# [ls-1] bin
# [ls-1] boot
# [ls-1] dev
# [ls-1] etc