from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    node1 = Node(
        package="8_launch",
        executable="5_launch_获取环境变量值",
        output="screen",
        additional_env={"TEST_VALUE": "abc123"},
    )

    node2 = Node(
        package="8_launch",
        namespace="test",
        executable="5_launch_获取环境变量值",
        output="screen",
        additional_env={"TEST_VALUE": "def456", "TEST2_VALUE": "ijk789"},
    )

    return LaunchDescription(
        [
            node1,
            node2,
        ]
    )


#  运行结果:
# [5_launch_获取环境变量值-2] USER: liuj , TEST_VALUE: def456, TEST2_VALUE: ijk789
# [5_launch_获取环境变量值-1] USER: liuj , TEST_VALUE: abc123, TEST2_VALUE: None
#
