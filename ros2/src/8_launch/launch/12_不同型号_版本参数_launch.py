import os
import json
from glob import glob
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    yk_param_config = os.path.join(
        get_package_share_directory("8_launch"), "model_A_param.yaml"
    )
    yt_s_param_config = os.path.join(
        get_package_share_directory("8_launch"), "model_B_param.yaml"
    )

    # 获取产品型号
    config_file = os.path.join(get_package_share_directory("8_launch"), "config.json")
    # config_file = "/workdir/config/config.json"

    # 方法1: 读取配置文件
    with open(config_file, "r") as fp:
        product = json.load(fp)["product"]
    # 方法2: 读取环境变量
    # product = os.environ.get("product")

    if product == "A":
        param_config = yk_param_config
    elif product == "B":
        param_config = yt_s_param_config
    else:
        raise ValueError(f"product must be A or B, {product}")
    print(f"param_config: {param_config}")

    demo_Node = Node(
        package="example",
        executable="demo",
        output="screen",
        parameters=[param_config],
    )

    return LaunchDescription(
        [
            demo_Node,
        ]
    )


""" 测试1: 产品A
配置 config/config.json
"product": "A",

ros2 launch 8_launch 12_不同型号_版本参数_launch.py
[demo-1] [INFO] [1732613686.114235539] [demo_get_param]: Name: Tom Age: 19 Source: 88.8
"""


""" 测试2: 产品B
配置 config/config.json
"product": "B",

ros2 launch 8_launch 12_不同型号_版本参数_launch.py
demo-1] [INFO] [1732613701.127328673] [demo_get_param]: Name: lilisi Age: 18 Source: 77.7
"""
