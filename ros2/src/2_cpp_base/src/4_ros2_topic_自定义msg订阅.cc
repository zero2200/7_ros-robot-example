#include <chrono>
#include <memory>

// ros2 include
#include "rclcpp/rclcpp.hpp"
#include "common/msg/num.hpp"

using namespace std;
using std::placeholders::_1;

class ros2_subscriber : public rclcpp::Node
{
private:
    rclcpp::Subscription<common::msg::Num>::SharedPtr sub;

public:
    ros2_subscriber()
        :Node("ros2_subscriber")
    {
        sub = this->create_subscription<common::msg::Num>("/topic_num", 10, std::bind(&ros2_subscriber::sub_callback, this, _1));
    }

    void sub_callback(const common::msg::Num::SharedPtr msg)
    {
        RCLCPP_INFO(this->get_logger(), "recv topic num %ld", msg->num);
    }
};

int main(int argc, char* argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ros2_subscriber>());
    rclcpp::shutdown();

    return 0;
}
