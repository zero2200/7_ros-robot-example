#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "common/msg/address_book.hpp"
#include "common/msg/contact.hpp"

using namespace std::chrono_literals;

class AddressBookPublisher : public rclcpp::Node
{
private:
  rclcpp::Publisher<common::msg::Contact>::SharedPtr address_book_publisher_;
  rclcpp::TimerBase::SharedPtr timer_;

public:
  AddressBookPublisher()
    : Node("address_book_publisher")
  {
    address_book_publisher_ =
      this->create_publisher<common::msg::Contact>("contact_address_book", 10);

    auto publish_msg = [this]() -> void {
      // auto msg = std::make_shared<common::msg::Contact>();
      auto msg = common::msg::Contact();
      {
        common::msg::AddressBook contact;
        contact.first_name = "John";
        contact.last_name = "Doe";
        contact.age = 30;
        contact.gender = contact.MALE;
        contact.address = "unknown";
        msg.address_book.push_back(contact);
      }
      {
        common::msg::AddressBook contact;
        contact.first_name = "Jane";
        contact.last_name = "Doe";
        contact.age = 20;
        contact.gender = contact.FEMALE;
        contact.address = "unknown";
        msg.address_book.push_back(contact);
      }

      address_book_publisher_->publish(msg);
      };
    timer_ = this->create_wall_timer(1s, publish_msg);
  }
};


int main(int argc, char* argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<AddressBookPublisher>());
  rclcpp::shutdown();

  return 0;
}