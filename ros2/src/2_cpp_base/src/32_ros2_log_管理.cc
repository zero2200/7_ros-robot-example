#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <iostream>
#include <rclcpp/rclcpp.hpp>

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    auto node = rclcpp::Node::make_shared("example_node");

    uint32_t count = 5;
    while (count)
    {
        RCLCPP_DEBUG(node->get_logger(), "ROS2 DEBUG count=%u", count);
        RCLCPP_INFO(node->get_logger(), "ROS2 INFO count=%u", count);
        RCLCPP_WARN(node->get_logger(), "ROS2 WARN count=%u", count);
        RCLCPP_ERROR(node->get_logger(), "ROS2 ERROR count=%u", count);
        RCLCPP_FATAL(node->get_logger(), "ROS2 FATAL count=%u", count);
        printf("c stdout count=%u\n", count);
        fprintf(stderr, "c stderr count=%u\n", count);
        std::cout << "c++ stdout count= " << count << std::endl;
        std::cerr << "c++ stderr count= " << count << std::endl;
        sleep(1);
        count --;
    }

    rclcpp::shutdown();
    return 0;
}

/* 
测试结论: 
1. DEBUG级别不输出
2. INFO,WARN,ERROR,FATAL级别输出

3. c/c++ stdout/stderr 不输出到日志文件, 只在屏幕输出log

 */