#include <unistd.h>
#include <rclcpp/rclcpp.hpp>

void condition_logging(std::shared_ptr<rclcpp::Node>& node)
{
    int count = 2;
    while(count){
        // 1. 只打印一次
        RCLCPP_INFO_ONCE(node->get_logger(), "1.只打印一次%d",count);

        // 2.除一次外, 其它打印
        RCLCPP_INFO_SKIPFIRST(node->get_logger(), "2.除第一次打印 %d",count);

        // 3.每秒打印一次, 1000ms 一次
        RCLCPP_ERROR_THROTTLE(node->get_logger(), *node->get_clock(), 1000, "3. 1秒=1000ms 打印一次 %d",count);
        RCLCPP_WARN_THROTTLE(node->get_logger(), *node->get_clock(), 500, "4. 0.5秒=500ms 打印一次 %d",count);
        usleep(500*1000);//500ms

        count --;
    }
}

void max_size_routeing(std::shared_ptr<rclcpp::Node>& node)
{
    int count = 10;
    while(count --)
    {
        RCLCPP_INFO(node->get_logger(), "123456789abcdefghijklmnokprst 大量的日志输出 %d",count);
    }
}

int main(int argc, char* argv[])
{
    rclcpp::init(argc, argv);
    auto node = rclcpp::Node::make_shared("example_node");

    int a = 42;
    double b = 3.14;
    std::string c = "Hello, world!";

    condition_logging(node);
    // max_size_routeing(node);

    // spdlog风格
    RCLCPP_INFO(node->get_logger(), "spdlog: a: {}, b: {}, c: {}", a, b, c);
    // printf style
    RCLCPP_INFO(node->get_logger(), "c printf: a=%d b=%f c=%s", a, b, c.c_str());
    // C++ stream style
    RCLCPP_INFO_STREAM(node->get_logger(), "c++ stream: a=" << a << " b=" << b << " c=" << c);

    // rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}