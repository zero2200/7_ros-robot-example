#include "rclcpp/rclcpp.hpp"
#include "common/srv/add_two_ints.hpp"

#include <memory>

void add(const std::shared_ptr<common::srv::AddTwoInts::Request> req,
    std::shared_ptr<common::srv::AddTwoInts::Response> resp)
{
    resp->sum = req->a + req->b;
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"add service %ld+%ld=%ld", req->a, req->b, resp->sum);
}

int main(int argc, char** argv)
{
    rclcpp::init(argc, argv);
    std::shared_ptr<rclcpp::Node> node = rclcpp::Node::make_shared("add_two_ints_server");
    rclcpp::Service<common::srv::AddTwoInts>::SharedPtr service = 
        node->create_service<common::srv::AddTwoInts>("add_two_ints",&add);

    rclcpp::spin(node);
    rclcpp::shutdown();
}
