#include <chrono>
#include <memory>

// ros2 include

#include "rclcpp/rclcpp.hpp"
#include "common/msg/num.hpp"

using namespace std;
using namespace std::chrono_literals;

class ros2_publisher : public rclcpp::Node
{
public:
    ros2_publisher()
        :Node("ros2_publisher"), count(0)
    {
        pub = this->create_publisher<common::msg::Num>("/topic_num", 10);
        timer = this->create_wall_timer(500ms, std::bind(&ros2_publisher::timer_callback, this));
    }

    void timer_callback()
    {
        common::msg::Num msg;
        msg.num = count++;
        RCLCPP_INFO(this->get_logger(), "send msg num: %ld", msg.num);
        pub->publish(msg);

    }

private:
    rclcpp::TimerBase::SharedPtr timer;
    rclcpp::Publisher<common::msg::Num>::SharedPtr pub;
    size_t count;
};

int main(int argc, char* argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<ros2_publisher>());
    rclcpp::shutdown();

    return 0;
}
