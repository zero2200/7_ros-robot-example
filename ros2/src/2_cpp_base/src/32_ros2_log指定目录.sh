#!/bin/bash

# 先执行: source install/setup.bash
# mkdir -p /autocity/data/logs
ros_dir=/tmp/log

# 1.日志目录
export ROS_LOG_DIR=$ros_dir
export ROS_LOG_TIME_FORMAT="%Y-%m-%d %H:%M:%S.%3f"
export RCL_LOGGING_IMPLEMENTATION=rcl_logging_spdlog
export SPDLOG_SETUP_CONF=/home/liuj/3_work/7_ros-robot-example/ros2/src/2_cpp_base/config/spdlog_setup.cfg
# export ROS_HOME=$ros_dir

# 2.格式化输出
# export RCUTILS_CONSOLE_OUTPUT_FORMAT="[{severity} {time} {function_name}] [{name}]: {message}"
# 3.终端输出颜色
# export RCUTILS_COLORIZED_OUTPUT=0
# 4.只输出到终端
# export RCUTILS_LOGGING_USE_STDOUT=0
# 5.行缓冲控制台输出
# export RCUTILS_LOGGING_BUFFERED_STREAM=1
# export ROS_LOG_CONFIG_FILE=/home/liuj/3_work/7_ros-robot-example/ros2/src/2_cpp_base/src/log_config_file.yaml
# -ros-args --log-file-name 31_log_output -->humble 不支持
    # --log-max-size 10K --log-backup-count 10 -->humble 不支持
    # --disable-stdout-logs  -->humble 支持
# ros2 run 2_cpp_base 31_log_output
ros2 run 2_cpp_base 32_ros2_log_manage
# stdbuf -oL -eL  ros2 run 2_cpp_base 32_ros2_log_manage &> run_32.log
