from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='2_cpp_base',
            executable='3_ros2_topic_selfMsg_Pub',
            name='topic_selfMsg_Pub',
        ),
        Node(
            package='2_cpp_base',
            executable='4_ros2_topic_selfMsg_Sub',
            name='topic_selfMsg_Sub',
        ),
    ])