import launch
import launch.actions
import launch.substitutions
from launch_ros.actions import Node


def generate_launch_description():
    return launch.LaunchDescription(
        [
            launch.actions.DeclareLaunchArgument(
                "node_prefix",
                default_value=[launch.substitutions.EnvironmentVariable("USER"), "_"],
                description="Prefix for node names",
            ),
            Node(
                package="2_cpp_base",
                executable="talker",
                output="screen",
                name=[
                    launch.substitutions.LaunchConfiguration("node_prefix"),"talker",
                    # f"{launch.substitutions.LaunchConfiguration("node_prefix")}_talker", --NG 
                ]
            ),
            Node(
                package="2_cpp_base",
                executable="listener",
                output="screen",
            ),
        ]
    )

""" 运行结果
ros2 node list
/liuj_talker

"""