import os
from glob import glob
from setuptools import find_packages, setup

package_name = '6_tf2_python'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*_launch.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='liuj',
    maintainer_email='lj1637664504@outlook.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            '1_tf2_静态广播 = 6_tf2_python.1_tf2_静态广播:main',
            '2_tf2_动态广播 = 6_tf2_python.2_tf2_动态广播:main',
            '3_tf2_监听程序 = 6_tf2_python.3_tf2_监听程序:main',
            '4_tf2_self_监听程序 = 6_tf2_python.4_tf2_self_监听程序:main',
            '5_tf2_fixed_frame_boradcast_固定帧广播 = 6_tf2_python.5_tf2_fixed_frame_boradcast_固定帧广播:main',
        ],
    },
)
