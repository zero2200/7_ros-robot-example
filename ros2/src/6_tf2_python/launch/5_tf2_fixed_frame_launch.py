import os

from ament_index_python.packages import get_package_share_directory,get_package_share_path,get_package_prefix

from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource

from launch_ros.actions import Node


def generate_launch_description():
    print("get_package_share_directory",get_package_share_directory("6_tf2_python"))
    print("get_package_share_path",get_package_share_path("6_tf2_python"))
    print("get_package_prefix",get_package_prefix("6_tf2_python"))
    demo_nodes = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([
            get_package_share_directory("6_tf2_python"),
            "/2_tf2_动态广播_launch.py",
        ])
    )

    return LaunchDescription([
        demo_nodes,
        Node(
            package="6_tf2_python",
            executable="5_tf2_fixed_frame_boradcast_固定帧广播",
            name="fixed_broadcaster",
        ),
    ])
