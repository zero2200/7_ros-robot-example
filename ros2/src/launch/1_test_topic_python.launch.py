from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='1_python_base',
            executable='1_topic_public',
            name='topic_public'
        ),
        Node(
            package='1_python_base',
            executable='2_topic_subscriber',
            name='topic_subscriber'
        ),
    ])