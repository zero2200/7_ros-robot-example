from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='2_cpp_base',
            executable='talker',
            name='talker_test'
        ),
        Node(
            package='2_cpp_base',
            executable='listener',
            name='listener_test'
        ),
    ])