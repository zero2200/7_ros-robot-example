import os
from glob import glob
from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription(
        [
            Node(
                package="3_py_srvcli",
                executable="service_add_two_ints",
                # namespace="release",
            ),
            Node(
                package="3_py_srvcli",
                namespace="debug",
                executable="service_add_two_ints",
                # name="liuj_service_add_two_ints",
                # remappings=[
                #     ("/topic", "/new_topic"),  # 设置话题的重映射
                # ],
            ),
        ]
    )
