import os
from glob import glob
from setuptools import find_packages, setup


package_name = '3_py_srvcli'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*_launch.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='liuj',
    maintainer_email='lj1637664504@outlook.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'service_add_two_ints = 3_py_srvcli.service_add_ints:main',
            'client_add_two_ints = 3_py_srvcli.client_add_ints:main',
            'py_client = 3_py_srvcli.py_client:main',
        ],
    },
)
