from common.srv import AddTwoInts
import rclpy
from rclpy.node import Node


class Add_Number_Service_Node(Node):
    def __init__(self):
        super().__init__("add_number_service")
        self.add_srv = self.create_service(AddTwoInts,"add_two_ints", self.add_two_ints_callback)

    def add_two_ints_callback(self, req, resp):
        resp.sum = req.a + req.b
        self.get_logger().info(
            "add_two_ints : %ld + %ld = %ld" % (req.a, req.b, resp.sum)
        )

        return resp


def main(args=None):
    rclpy.init(args=args)
    add_number_service_node = Add_Number_Service_Node()
    rclpy.spin(add_number_service_node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
