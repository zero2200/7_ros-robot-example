from setuptools import find_packages, setup

package_name = '5_python_action'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='liuj',
    maintainer_email='lj1637664504@outlook.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            '1_python_action_server_基础_触发调用 = 5_python_action.1_python_action_server_基础_触发调用:main',
            '2_python_action_server_返回结果 = 5_python_action.2_python_action_server_返回结果:main',
            '3_python_action_server_返回feedback = 5_python_action.3_python_action_server_返回feedback:main',
            '21_python_action_client_触发action = 5_python_action.21_python_action_client_触发action:main',
            '22_python_action_client_获取结果 = 5_python_action.22_python_action_client_获取结果:main',
            '23_python_action_client_带反馈feedback = 5_python_action.23_python_action_client_带反馈feedback:main',
        ],
    },
)
