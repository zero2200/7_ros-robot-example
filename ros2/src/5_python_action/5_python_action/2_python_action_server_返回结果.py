import rclpy
from rclpy.node import Node
from rclpy.action import ActionServer

from common.action import Fibonacci


class TestActionServer(Node):

    def __init__(self):
        super().__init__("test_action_server")
        self.action_server = ActionServer(
            self, Fibonacci, "fibonacci", self.execute_callback
        )

    def execute_callback(self, goal_handle):
        self.get_logger().info("Execute action server")
        num = [0,1]
        for i in range(1,goal_handle.request.order):
            num.append(num[i]+num[i-1])
        goal_handle.succeed()
        result = Fibonacci.Result()
        result.sequence = num
        return result


def main(args=None):
    rclpy.init(args=args)
    action_server_node = TestActionServer()
    rclpy.spin(action_server_node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()


""" 
测试指令: ros2 action send_goal fibonacci common/action/Fibonacci "{order: 5}"
"""