import time
import rclpy
from rclpy.node import Node
from rclpy.action import ActionServer, ActionClient

from common.action import Fibonacci


class TestActionClient(Node):

    def __init__(self):
        super().__init__("test_action_client")
        self.action_client = ActionClient(self, Fibonacci, "fibonacci")

    def send_goal(self, order):
        goal_msg = Fibonacci.Goal()
        goal_msg.order = order
        self.action_client.wait_for_server()
        return self.action_client.send_goal_async(goal_msg)


def main(args=None):
    rclpy.init(args=args)
    action_client_node = TestActionClient()
    future = action_client_node.send_goal(5)
    rclpy.spin_until_future_complete(action_client_node, future)


if __name__ == "__main__":
    main()


""" 
等同测试指令: 
ros2 action send_goal fibonacci common/action/Fibonacci "{order: 5}"
"""
