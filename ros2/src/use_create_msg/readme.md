需求:
同一个包, 引用包中的msg

方法:

cmake添加引用自身msg头文件方法
```diff
find_package(rosidl_default_generators REQUIRED)
rosidl_generate_interfaces(${PROJECT_NAME}
  "msg/Test8.msg"
  DEPENDENCIES std_msgs  # Add packages that above messages depend on, in this case geometry_msgs for Sphere.msg
)
++ ament_export_dependencies(rosidl_default_runtime)

add_executable(1_topic_public src/1_topic_public.cc)
ament_target_dependencies(1_topic_public rclcpp std_msgs)
++ rosidl_target_interfaces(1_topic_public
++   ${PROJECT_NAME} "rosidl_typesupport_cpp")

install(TARGETS
  1_topic_public
  DESTINATION lib/${PROJECT_NAME})
```