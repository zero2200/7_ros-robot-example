import os
from glob import glob
from setuptools import find_packages, setup

package_name = '7_multi_file_setup'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        ('', glob('scripts/*')),
        (os.path.join('lib', package_name), glob('7_multi_file_setup/*.py')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='liuj',
    maintainer_email='lj1637664504@outlook.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'main_node = 7_multi_file_setup.main_node:main',
            '2_添加模块路径_append_module_dir = 7_multi_file_setup.2_添加模块路径_append_module_dir:main',
        ],
    },
)
