import os
import sys


def ros_append_module_dir():
    # 获取当前脚本的绝对路径
    script_path = os.path.abspath(__file__)
    # 获取当前脚本所在的目录
    script_dir = os.path.dirname(script_path)
    sys.path.append(script_dir)


ros_append_module_dir()

# 导入本地模块
from file1 import *


def main():
    print("file1_config:", file1_config)


if __name__ == "__main__":
    main()
