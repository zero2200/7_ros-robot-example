#!/bin/bash
args=""
common_flag=0
tasknum=8
while getopts "p:vgij:c" arg
do
	case $arg in
	p)
		args="$args --packages-select $OPTARG"
        ;;
	g)
		args="$args --cmake-args -DCMAKE_BUILD_TYPE=Debug"
		;;
	v)
		args="$args --event-handlers console_cohesion+ --cmake-args -DCMAKE_VERBOSE_MAKEFILE=ON"
		;;
    i)
        # args=${args/--symlink-install/}
		args="$args --symlink-install"
        ;;
    j)
        tasknum=$OPTARG
        ;;
    c)
        common_flag=1
        ;;
	?)
		echo "error: unkonw argument"
		exit 1
	esac
done


function build_common()
{
	if [ $common_flag -eq 1 ];then
		colcon build --packages-select chassis_msgs  control_msgs perception_msgs hmi_msgs monitor_msgs plan_msgs shm_msgs common_msgs msfl_msgs routing_msgs --parallel-workers 8
		exit 0
	fi
}
build_common

echo "colcon build $args --parallel-workers $tasknum"
colcon build $args --parallel-workers $tasknum
# 指定使用的cpu
# taskset -c 0-7 colcon build $args
