from enum import IntEnum
from dataclasses import dataclass

class Ros_Log_Type(IntEnum):
    Screen = 1
    Log = 2
    Screen_Log = 4

@dataclass
class Log_Config:
    Log_Dir: str = "/autocity/data/logs"
    Log_Symlink_Dir: str = "/autocity/data/log"
    Log_Index_File: str = "/autocity/data/ros_log_index"

@dataclass
class Sweeper_Model_Config:
    Model_Name: str
    Node_List: list
    log_config: Log_Config

@dataclass
class Ros_Node_Attr:
    node_dir: str
    start_cmd: str
    stop_cmd: str
    log_type: Ros_Log_Type = Ros_Log_Type.Screen_Log

sweeper_model_a = Sweeper_Model_Config(
    "model_a",
    [
        # Ros_Node_Attr("test", "start.sh", "stop.sh", log_type=Ros_Log_Type.Screen), #示例
        Ros_Node_Attr("test", "start.sh", "stop.sh"), #示例
        Ros_Node_Attr("test2", "start.sh", "stop.sh"),
        # Ros_Node_Attr("control", "start.sh", "stop.sh"),
        # Ros_Node_Attr("hmi", "start.sh", "stop.sh"),
        # Ros_Node_Attr("location", "start.sh", "stop.sh"),
        # Ros_Node_Attr("planning", "start.sh", "stop.sh"),
        # Ros_Node_Attr("routing", "start.sh", "stop.sh"),
        # Ros_Node_Attr("perception1", "start.sh", "stop.sh"),
    ],
    Log_Config("/autocity/data/logs", "/autocity/data/log", "/autocity/data/ros_log_index"),
)
